#!/bin/bash

uploadselector=(*.xclbin)
uploadfile=${uploadselector[0]}
echo "Uploading file $uploadfile"

projectid='38967823'
package='xclbin'
version='latest'

tockenfile='.tocken'
n=1
while read line; do
    if [[ $n -eq 1 ]]
    then
        username=$line
    else
        password=$line
    fi
    n=$((n+1))
done < $tockenfile

echo "Uploading as user $username"

curl --user $username:$password \
    --upload-file $uploadfile \
    "https://gitlab.com/api/v4/projects/$projectid/packages/generic/$package/$version/$uploadfile?select=package_file"

echo
echo "Succesfully uploaded"